/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sgc
 */
public class ChatSocket extends Thread {
    Socket socket;
    public ChatSocket(Socket s) {
        this.socket =s;  
    }
    
    public void out(String out) {
        try {
            socket.getOutputStream().write(out.getBytes("UTF-8"));
        } catch (IOException ex) {
            Logger.getLogger(ChatSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void run() {
        int count = 0;
        while (true) {
            count++;
            out("loop"+count);   
        }
    }
    
}
